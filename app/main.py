from fastapi import FastAPI, Response, Request
from starlette import status

from app.db.database import check_db_connection

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


# Kubernetes Ready and Liveness Probes
@app.get("/live", status_code=200)
def live_probe():
    return {"app": "Live!"}


@app.get("/ready")
def ready_probe(response: Response):
    database_ready: bool = check_db_connection()
    if database_ready:
        response.status_code = status.HTTP_200_OK
        return {"app": "Ready!"}
    else:
        # Can be anything but 200
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE
        return {"app": "Stuck!"}
