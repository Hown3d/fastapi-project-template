###############################################
# Base Image
###############################################
FROM python:3.9-slim-buster as python-base

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

# prepend poetry and venv to path
ENV PATH="$VENV_PATH/bin:$PATH"

###############################################
# Builder Image
###############################################
FROM python-base as builder-base

# install poetry - respects $POETRY_VERSION & $POETRY_HOME
RUN pip install poetry

# copy project requirement files here to ensure they will be cached.
WORKDIR $PYSETUP_PATH
COPY poetry.lock pyproject.toml ./

# install runtime deps - uses $POETRY_VIRTUALENVS_IN_PROJECT internally
RUN poetry install --no-dev --no-interaction --no-ansi

###############################################
# Production Image
###############################################
FROM python-base as production

ENV USER=appuser \
    GROUP=appgroup

WORKDIR /code

# Add a new group and user so that the docker doesnt run as root
RUN addgroup ${GROUP} && adduser ${USER} --ingroup appgroup

#change user to newly created user
USER ${USER}

COPY --from=builder-base --chown=${USER}:${GROUP} $PYSETUP_PATH $PYSETUP_PATH
COPY --chown=${USER}:${GROUP} app/ app/

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0"]