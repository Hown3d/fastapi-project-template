# FastAPI Project Template

A template to run a containerized FastAPI application with skaffold and kubernetes

## Skaffold
- Skaffold is a tool to simplify the build and deploy mechanism
- to test the application in a local development use `skaffold dev`
  - create a .dockerconfig.json in infra and add your deploy token to the **auth** section.
    - an example is located under docs/skaffold
    - gitlab deploy tokens can be created in the repository page under **settings**->**repository**->**deploy tokens**
  
  
## Testing your application
- Open a terminal into your pod and execute `PYTHONPATH=. pytest`.

## Adding python dependencies
- make sure to have poetry installed on your machine
- `poetry add <PACKAGE_NAME>` or `poetry add -D <PACKAGE_NAME>` to install either
  into production dependencies or development dependencies with -D

## Kubernetes Gitlab Integration
- Using your kubernetes cluster with gitlab to automaticly create namespaces add your cluster under **Operations** -> **Kubernetes**
- create a gitlab service account using this [manifest](infra/gitlab-service-account.yaml) 
  - `kubectl apply -f <MANIFEST>`

## Semantic Version
- This project supports Semantic versioning using [semantic-release](https://github.com/semantic-release/semantic-release)
- a new version is automaticly created by the ci/cd pipeline whenever the master branch recieves a new commit
- in order to use this feature, make sure to use the [Angular Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format)
- create a Access Token for Semantic Release to create a version in your repository
  - **Edit Profile** -> **Access Tokens** -> Token with scopes: api, read_repository, write_repository
  - add this token into your CI/CD Variables under the name **GL_TOKEN**

Example how the version is generated:
| Commit message | Release type |
| ------ | ------ |
| fix(pencil): stop graphite breaking when too much pressure applied | Patch Release
| feat(pencil): add 'graphiteWidth' option |Minor Feature Release
| perf(pencil): remove graphiteWidth option <br><br>BREAKING CHANGE: The graphiteWidth option has been removed. <br>The default graphite width of 10mm is always used for performance reasons. | Major Breaking Release 

## CI/CD Pipeline
![ci_cd](docs/ci_cd_pipeline.png)
